//
//  Person.swift
//  Singleton
//
//  Created by CavanSu on 2018/7/30.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import UIKit

class Person: NSObject {
    static let person = Person()
    
    static func sharePerson() -> Person {
        return person
    }
}
