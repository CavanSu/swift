//
//  ContentView.swift
//  SwiftUI-Tutorial
//
//  Created by CavanSu on 2019/7/25.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    var body: some View {
        Text("Hello World")
            .font(Font.largeTitle)
            .color(Color.green)
    }
}

//#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
//#endif
