//
//  main.swift
//  16- ARC
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

#if false
// 1. 强引用与弱引用
// 值会被自动初始化为nil，目前还不会引用到Person类的实例
var reference1: Person?
var reference2: Person?
weak var reference3: Person? // weak 的引用计数 + 1

// 创建Person类的新实例
reference1 = Person(name: "Runoob")
//赋值给其他两个变量，该实例又会多出两个强引用
reference2 = reference1
reference3 = reference1

//断开第一个强引用
print("断开第一个强引用")
reference1 = nil
//断开第二个强引用, 并调用析构函数
print("断开第二个强引用")
reference2 = nil
//断开第三个弱引用，
print("断开第三个强引用")
reference3 = nil

    
// 强引用造成的循环

// 两个变量都被初始化为nil
var runoob: ApartPerson?
var number73: Apartment?

// 赋值
runoob = ApartPerson(name: "Runoob")
number73 = Apartment(number: 73)

// 感叹号是用来展开和访问可选变量 runoob 和 number73 中的实例
// 循环强引用被创建
runoob!.apartment = number73
number73!.tenant = runoob

// 断开 runoob 和 number73 变量所持有的强引用时，引用计数并不会降为 0，实例也不会被 ARC 销毁
// 注意，当你把这两个变量设为nil时，没有任何一个析构函数被调用。
// 强引用循环阻止了Person和Apartment类实例的销毁，并在你的应用程序中造成了内存泄漏
runoob = nil
number73 = nil
#endif






#if true
// 2.无主引用 -> unowened 与 weak 相似不会使引用计数 +1，但有区别，如下
//   如果捕获的引用绝对不会置为nil，应该用无主引用，而不是弱引用。
    class C{
        
        var name:String
//        lazy var block:()->() = {[unowned self] in
//            print(self.name)
//        }
        // weak 与 unowened 的区别， 当使用weak时，self为可选值，unowned时self为非可选值
        lazy var block:()->() = {[weak self] in
            print(self?.name ?? "self.name = nil") // ?? "self.name = nil", 当self?.name= nil时，提供一个默认值
        }
        
        init(name:String) {
            self.name = name
            print("C Init")
        }
        
        deinit {
            print("C deinit")
        }
    }
    
    class D{
        var block:(()->())!
        
        init(callBack:(()->())?) {
            self.block = callBack!
            print("D Init")
        }
        
        deinit {
            print("D deinit")
        }
    }
    
var c:C? = C(name:"c")
var d = D.init(callBack:c?.block)
    
c!.block()
c = nil
d.block()
#endif
