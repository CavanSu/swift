//
//  main.swift
//  06- Function
//
//  Created by CavanSu on 19/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

func test()  {
    print("test")
}

// MARK: - Swift 支持多态
func includeReturnExSample(sampleName: String) -> String {
    return sampleName
}

func includeReturnExSample(sampleName: String, second: String) -> String {
    return sampleName + second
}


// MARK: - 带返回值的方法
func minOfArray(array: [Int]) -> (min: Int, index: Int) {
    var min: Int = 0
    var index: Int = 0
    
    // index在前， value在后
    for (i, value) in array.enumerated() {
        if min > value {
            min = value
            index = i
        }
    }
    
    return (min, index)
}

func maxOfArray(array: [Int]) -> (max: Int, index: Int)? {
    var max: Int = 0
    var ind: Int = 0
    
    // index在前， value在后
    for (i, value) in array.enumerated() {
        if max < value {
            max = value
            ind = i
        }
    }
    
    return (max, ind)
}

// MARK: -
// firstName: 外部参数名， 在外部调用时可以看到，
// a: 内部参数名，在方法内部使用的参数
func exterParaName(firstName a: Int, secondName b: Int) {
    print("firstName a: \(a), secondName b: \(b)")
}


// MARK: - 改变参数的值，交换两个参数的值
func swapTwoInts(_ a: inout Int, _ b: inout Int) {
    print("a: \(a), b: \(b)")
    
    let temporaryA = a
    a = b
    b = temporaryA
    
    print("a: \(a), b: \(b)")
}

// MARK: - 可变参数
// 可变参数可以接受零个或多个值。函数调用时，你可以用可变参数来指定函数参数，其数量是不确定的。
// 可变参数通过在变量类型名后面加入（...）的方式来定义。
func vari<N>(members: N...){
    print("可变参数\n")
    for i in members {
        print(i)
    }
}


//MARK: Above Definition of Funtions

// 1. 不带参数与返回值
test()

// 2. 带参数与返回值
print(includeReturnExSample(sampleName: "La La La"))

// 3. 多参数
print(includeReturnExSample(sampleName: "La La La", second: " 2"))

// 4. 返回值是元组
let result = minOfArray(array: [-1, 2, -3, 4])
print("min: \(result.min), index: \(result.index)")

// 5. 返回值是可选元组
let resultOp = maxOfArray(array: [-1, 2, -3, 4])
print("max: \(String(describing: resultOp?.max)), index: \(String(describing: resultOp?.index))")

if let bound = resultOp {
    print("max: \(bound.max), index: \(bound.index)")
}

// 6. 外部参数名
exterParaName(firstName: 2, secondName: 3)

// 7. 改变参数的值，交换两个参数的值
var swapA = 5, swapB = 10
swapTwoInts(&swapA, &swapB)


// 8. 可变参数
vari(members: 4.5, 3.1, 5.6)
vari(members: "Google", "Baidu", "Runoob")
//vari(members: 1, "int and string") 不能混合类型
