//
//  main.swift
//  Array
//
//  Created by CavanSu on 2019/1/2.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import Foundation

func initArray() {
    // 以下实例创建了一个类型为 Int ，数量为 5，初始值为 10 的数组：
    // 这个方法，不是copy 5份， 而是存储5个地址(5个相同的地址)
    let someInts = [Int](repeating: 10, count: 5)
    
    //        let someInts : [Int] = [10, 20, 30]
    //        let someInts = [10, 20, 30]
    
    for item in someInts {
        print("int : \(item)")
    }
    
    let someVar = someInts[0]
    print( "第一个元素的值 \(someVar)" )
    print( "第二个元素的值 \(someInts[1])" )
    print( "第三个元素的值 \(someInts[2])" )
}

func motifiedArray() {
    var someInts = [Int]()
    
    // 数组增加元素
    someInts.append(20)
    someInts.append(30)
    someInts += [40]
    
    let someVar = someInts[0]
    
    // 修改最后一个元素
    someInts[2] = 50
    
    print( "第一个元素的值 \(someVar)" )
    print( "第二个元素的值 \(someInts[1])" )
    print( "第三个元素的值 \(someInts[2])" )
    
    // 移除一个元素
    someInts.remove(at: 0)
    print( "第一个元素的值 \(someInts[0])" )
}

func enumerateArray() {
    var someStrs = [String]()
    
    someStrs.append("Apple")
    someStrs.append("Amazon")
    someStrs.append("Runoob")
    someStrs += ["Google"]
    
    // isEmpty
    print("someStrs.isEmpty = \(someStrs.isEmpty)")
    
    for (index, item) in someStrs.enumerated() {
        print("在 index = \(index) 位置上的值为 \(item)")
    }
    
    for item in someStrs {
        print("item: \(item)")
    }
}

func mixingTwoArray() {
    let intsA = [Int](repeating: 0, count:5)
    let intsB = [Int](repeating: 1, count:3)
    let intsC = intsA + intsB
    
    // count
    print("count: \(intsC.count)")
    
    for item in intsC {
        print(item)
    }
    
    // convert to string
    let string = intsC.map(String.init)
    let final = string.joined(separator: "")
    print("string: \(final)")
}





//        initArray()
//        motifiedArray()
//        enumerateArray()
mixingTwoArray()
