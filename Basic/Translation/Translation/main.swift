//
//  main.swift
//  17- Translation
//
//  Created by CavanSu on 22/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

let sa = [ Chemistry(physics: "固体物理", equations: "赫兹"),
           Maths(physics: "流体动力学", formulae: "千兆赫"),
           Chemistry(physics: "热物理学", equations: "分贝"),
           Maths(physics: "天体物理学", formulae: "兆赫"),
           Maths(physics: "微分方程", formulae: "余弦级数")]

// 1. 确定是什么类型
#if false

let samplechem = Chemistry(physics: "固体物理", equations: "赫兹")
print("实例物理学是: \(samplechem.physics)")
print("实例方程式: \(samplechem.equations)")


let samplemaths = Maths(physics: "流体动力学", formulae: "千兆赫")
print("实例物理学是: \(samplemaths.physics)")
print("实例公式是: \(samplemaths.formulae)")
    
for (index, item) in sa.enumerated() {
    if item is Chemistry {
        print("\(index) is Chemistry")
    }
    else {
        print("\(index) is Maths")
    }
}
#endif



// 2. 类型转化
#if false

for item in sa {
    // as? -> 不确定转化，当可以转化时返还相应的值，不能转化时返回Nil
    if let show = item as? Chemistry {
        print("化学主题是: '\(show.physics)', \(show.equations)")
        
    }
    else {
        // as! -> 强制转化，一旦不能转化直接Crash
        let example = item as! Maths
        print("数学主题是: '\(example.physics)',  \(example.formulae)")
    }
}
#endif


// 3. Any和AnyObject的类型转换

/*
 Swift为不确定类型提供了两种特殊类型别名：
    1. AnyObject可以代表任何class类型的实例。
    2. Any可以表示任何类型，包括方法类型（function types)
 */

// Any 实例
// 可以存储Any类型的数组 exampleany

#if false

var exampleany = [Any]()

exampleany.append(12)
exampleany.append(3.14159)
exampleany.append("Any 实例")
exampleany.append(Chemistry(physics: "固体物理", equations: "兆赫"))

for item2 in exampleany {
    switch item2 {
        
        // 每个item2 的类型判断
    case let someInt as Int:
        print("整型值为 \(someInt)")
    case let someDouble as Double where someDouble > 0:
        print("Pi 值为 \(someDouble)")
    case let someString as String:
        print("\(someString)")
    case let phy as Chemistry:
        print("主题 '\(phy.physics)', \(phy.equations)")
    default:
        print("None")
    }
}

print("\n[AnyObject] 类型的数组")

// [AnyObject] 类型的数组
let saprint: [AnyObject] = [Chemistry(physics: "固体物理", equations: "赫兹"),
                            Maths(physics: "流体动力学", formulae: "千兆赫"),
                            Chemistry(physics: "热物理学", equations: "分贝"),
                            Maths(physics: "天体物理学", formulae: "兆赫"),
                            Maths(physics: "微分方程", formulae: "余弦级数")]

for item in saprint {
    
    if let show = item as? Chemistry {
        print("化学主题是: '\(show.physics)', \(show.equations)")
        
    } else if let example = item as? Maths {
        print("数学主题是: '\(example.physics)',  \(example.formulae)")
    }
}
#endif

