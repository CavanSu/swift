//
//  Class.swift
//  17- Translation
//
//  Created by CavanSu on 22/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

class Subjects {
    var physics: String
    init(physics: String) {
        self.physics = physics
    }
}

class Chemistry: Subjects {
    var equations: String
    init(physics: String, equations: String) {
        self.equations = equations
        super.init(physics: physics)
    }
}

class Maths: Subjects {
    var formulae: String
    init(physics: String, formulae: String) {
        self.formulae = formulae
        super.init(physics: physics)
    }
}


