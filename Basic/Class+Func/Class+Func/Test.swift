//
//  Test.swift
//  11- Class+Func
//
//  Created by CavanSu on 20/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Cocoa

class Test: NSObject {
    func test1(a: Int) {
        print("test1")
    }
    
    func test2(a: Int, b: Int) {
        print("test2")
    }
    
    func test3(a: inout Int, b: inout Int) {
        print("test3")
    }
    
    // func, 前添加 class， 子类就不能继承这个方法
    class func test4(firstName a: Int, secondName b: Int) {
        print("test4")
    }
    
    // 类方法
    static func classTest() {
        print("classTest")
    }
}
