//
//  main.swift
//  09- Struct
//
//  Created by CavanSu on 19/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

let marks = MarkStruct()
print("Mark1 是 \(marks.mark1)")
print("Mark2 是 \(marks.mark2)")
print("Mark3 是 \(marks.mark3)")


let scoreStruct = ScoreStruct(Default: 123)
print("score: \(scoreStruct.score)")

// 构造函数的赋值优先级高于 默认值
let marks1 = MarkStruct(mark1: 3, mark2: 1, mark3: 2)
print("Mark1 是 \(marks1.mark1)")
print("Mark2 是 \(marks1.mark2)")
print("Mark3 是 \(marks1.mark3)")

// Attention
// 这里是拷贝不是指向，拷贝出一个新的结构体
// 任何在结构体中储存的值类型属性，也将会被拷贝，而不是被引用。
let scoreStruct2 = scoreStruct


// 打印地址
print("address: \(Unmanaged<AnyObject>.passUnretained(scoreStruct as AnyObject).toOpaque())")
print("address: \(Unmanaged<AnyObject>.passUnretained(scoreStruct2 as AnyObject).toOpaque())")

