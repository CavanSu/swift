//
//  Mark.swift
//  09- Struct
//
//  Created by CavanSu on 19/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

struct MarkStruct{
    var mark1 = 100
    var mark2 = 78
    var mark3 = 98
}

struct ScoreStruct{
    var score: Int
    
    init(Default score: Int) {
        self.score = score
    }
}
