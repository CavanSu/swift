//
//  main.swift
//  01-HelloWorld
//
//  Created by CavanSu on 2018/11/2.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// MARK: print
var myString = "Hello, World!"
print(myString)
print("First: \(myString)")

// MARK: 变量
// 变量声明意思是告诉编译器在内存中的哪个位置上为变量创建多大的存储空间。
// 使用 var 关键字声明它, 程序运行时可以改变值

var varA = 42
print(varA)

varA = 44
print(varA)

var varB:Float

varB = 3.14159
print(varB)


// MARK: 常量
// 常量一旦设定，在程序运行时就无法改变其值
let constA = 42
print(constA)


// MARK: 类型标注
// 当声明常量或者变量的时候可以加上类型标注
let constB:Float = 3.14159
print("constB: \(constB)")
