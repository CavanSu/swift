//
//  main.swift
//  String
//
//  Created by CavanSu on 2018/11/2.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// 使用字符串字面量
let stringA = "Hello, World!"
print(stringA)

// String 实例化
let stringB = String("Hello, World!")
print(stringB)


// 使用字符串字面量创建空字符串
let stringC = ""

if stringC.isEmpty { // isEmpty，字符串长度为0
    print("stringC's length is Empty")
} else {
    print("stringC's length is not Empty")
}

// MARK: 实例化 String 类来创建空字符串
let stringD = String()

if stringD.isEmpty {
    print("stringD's length is Empty")
} else {
    print("stringD's length is not Empty")
}


// MARK: 
let varA   = 20
let constA = 100
let varC:Float = 20.0

let stringE = "\(varA) 乘于 \(constA) 等于 \(varC * 100)"
print(stringE)

print("stringE's count : \(stringE.count)")


// MARK: 字符串比较
var varD = "Hello, Swift!"
let varB = "Hello, World!"

if varD == varB { // 可以通过 == 直接判断字符串是否相等
    print("\(varD) 与 \(varB) 是相等的")
} else {
    print("\(varD) 与 \(varB) 是不相等的")
}


// MARK: UTF-8, UTF-16
var unicodeString = "苏若晞"

print("UTF-8 : ")
for code in unicodeString.utf8 {
    print("\(code)")
}

print("\n")

print("UTF-16 : ")
for code in unicodeString.utf16 {
    print("\(code)")
}


// MARK: 遍历字符串中的字符
for ch in "Runoob" {
    print(ch)
}


// MARK: 字符串连接字符, 字符串连接字符串
varD.append("Z")
print("\(varD)")

varD = varD + varB
print("\(varD)")

// MARK: 字符串长度
print("string length: \(varD.count)")


// MARK: SubString
print("----------------------SubString-----------------------")

var str = "Hello, playground"

// Beginning of a string

var index = str.index(str.startIndex, offsetBy: 5)
var mySubstring = str[str.startIndex..<index] // Hello
print("mySubstring: \(mySubstring)")

// or
mySubstring = str.prefix(5)
print("mySubstring: \(mySubstring)")


// End of a string

index = str.index(str.endIndex, offsetBy: -10)
mySubstring = str[index...] // playground
print("mySubstring: \(mySubstring)")

// or
index = str.index(str.endIndex, offsetBy: -10)
mySubstring = str.suffix(from: index) // playground
print("mySubstring: \(mySubstring)")

// or
mySubstring = str.suffix(10) // playground
print("mySubstring: \(mySubstring)")

// Range of a string

let start = str.index(str.startIndex, offsetBy: 7)
let end = str.index(str.endIndex, offsetBy: -6)
let range = start..<end

mySubstring = str[range]  // play
print("mySubstring: \(mySubstring)")
