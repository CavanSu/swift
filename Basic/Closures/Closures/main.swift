//
//  main.swift
//  Closures
//
//  Created by CavanSu on 19/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// 返回值为一个不带参数的: incrementor
func makeIncrementor(forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    func incrementor() -> Int {
        runningTotal += amount
        return runningTotal
    }
    return incrementor
}


// MARK: - Above Definition of Funtions

// 1. 最简单闭包
let closeTest = { print("1. Closures test") }
closeTest()


// 2. 两个参数，且带返回值
let divide = {(val1: Int, val2: Int) -> Int in
    return val1 / val2
}
let result = divide(200, 20)
print ("2. result: \(result)")


// 3. Swift 标准库提供了名为 sorted(by:) 的方法，会根据您提供的用于排序的闭包函数将已知类型数组中的值进行排序。
//    排序完成后，sorted(by:) 方法会返回一个与原数组大小相同，包含同类型元素且元素已正确排序的新数组。
//    原数组不会被 sorted(by:) 方法修改。
let names = ["1", "3", "2", "8", "0"]

// 使用普通函数(或内嵌函数)提供排序功能,闭包函数类型需为(String, String) -> Bool。
// 闭包函数 backwards(s1: String, s2: String) -> Bool ，
// 该闭包函数需要传入与数组元素类型相同的两个值，并返回一个布尔类型值来表明当排序结束后传入的第一个参数排在第二个参数前面还是后面。如果第一个参数值出现在第二个参数值前面，排序闭包函数需要返回 true，反之返回 false。
func backwards(s1: String, s2: String) -> Bool {
    return s1 > s2
}

var reversed = names.sorted(by: backwards)
print("3.1 reversed: \(reversed)")

// $0, 特指为参数1, $1 为参数2
var reversed2 = names.sorted(by: { $0 < $1 })
print("3.2 reversed2: \(reversed2)")

//
var reversed3 = names.sorted(by: < )
print("3.3 reversed3: \(reversed3)")

// 4. 尾随闭包
var reversed4 = names.sorted(){ $0 > $1 }
print("4.1 reversed4: \(reversed4)")

// 尾随闭包甚至可以这样！()都去掉
var reversed5 = names.sorted{ $0 > $1 }
print("4.2 reversed5: \(reversed5)")

// 5. 闭包作为返回值
let incrementByTen = makeIncrementor(forIncrement: 10)

// 返回的值为10, runningTotal 被闭包持有，所以不断叠加
print(incrementByTen())

// 返回的值为20
print(incrementByTen())

// 返回的值为30
print(incrementByTen())

















