//
//  main.swift
//  13- Constructors
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// 1.
let rec = Rectangle()
print("1. Area: \(rec.length * rec.breadth)")



// 2.
let cir = Circle(radius: 5)
// Q: 为什么 var 后面不需要添加 ! , ?
// A: 只能使用带参数给属性赋值的构造方法
print("2. radius: \(cir.radius)")

// A: 或者 init() 方法里包含给属性设置
// 总之初始化时会检查属性是否被赋值，不被赋值就会报错
let dog = Dog()


// 3. 可失败构造方法
let someCreature = Animal(species: "")
if let giraffe = someCreature {
    print("Animal init: \(giraffe.species)")
}
else {
    print("Animal init fail")
}


// 4. Class不显式声明 继承与谁， 则默认继承于NSObject
let sc = SuperClass()


// 5. 便利构造器实例
let baby = BabyClass(no1: 10)
print("5. baby.no1: \(baby.no1), baby.no2: \(baby.no2)")


// 6. 枚举的初始化
let fahrenheitUnit = TemperatureUnit(symbol: "F")

if let fahrenheitUnit = fahrenheitUnit {
    print("6.1 Init Success, type: \(fahrenheitUnit.description())")
}


let unknownUnit = TemperatureUnit(symbol: "X")

if let unknownUnit = unknownUnit {
    print("6.2 Init Success, type: \(unknownUnit.description())")
}
else {
    print("6.3 Init Fail")
}


