//
//  Rectangle.swift
//  13- Constructors
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

struct Rectangle {
//    var length: Double = 10
//    var breadth: Double = 10
//    var area: Double = 10
//
//    init() {
//        print("overwrite init, no error")
//    }
    
    
    // 重写init()时， 当属性在声明时没有赋值则在init()里需要赋值，否则会报错, 反之不会，参考上面注释
    var length: Double
    var breadth: Double
    var area: Double
    
    
    init() {
        length = 6
        breadth = 12
        area = 0
    }
    
    // 可以存在多个 init()方法
    init(fromLength length: Double, fromBreadth breadth: Double) {
        self.length = length
        self.breadth = breadth
        area = length * breadth
    }

    init(fromLeng leng: Double, fromBread bread: Double) {
        self.length = leng
        self.breadth = bread
        area = leng * bread
    }
    
}

struct Circle {
    var radius: Double
    // 系统会默认提供两个默认的 init 方法
    // 1. init()
    // 2. init(radius: Double)
}


struct Dog {
    var age: Int
    init() {
        age = 10
    }
}




// 可失败构造器
// 可以在一个类，结构体或是枚举类型的定义中，添加一个或多个可失败构造器。其语法为在init关键字后面加添? or !(init? or !)
struct Animal {
    let species: String
    init!(species: String) {
        if species.isEmpty { return nil }
        self.species = species
    }
}

