//
//  Enum.swift
//  13- Constructors
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

enum TemperatureUnit {
    // 开尔文，摄氏，华氏
    case Kelvin, Celsius, Fahrenheit
    
    init?(symbol: Character) {
        switch symbol {
        case "K":
            self = .Kelvin
        case "C":
            self = .Celsius
        case "F":
            self = .Fahrenheit
        default:
            return nil
        }
    }
    
    func description() -> String {
        switch self {
        case  .Kelvin:     return ".Kelvin"
        case  .Celsius:    return ".Celsius"
        case  .Fahrenheit: return ".Fahrenheit"
        }
    }
}

