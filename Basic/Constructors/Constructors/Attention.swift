//
//  Attention.swift
//  13- Constructors
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

class Cat {
    var name: String
    init() {
        name = "cat"
    }
}

// 需要保证在当前子类实例的成员初始化完成后才能调用父类的初始化方法：
class Tiger: Cat {
    let power: Int
    override init() {
        power = 10
        super.init()
        name = "tiger"
        
//        power = 10
    }
}
