//
//  Class.swift
//  13- Constructors
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

class SuperClass {
    var corners = 4
    var width: Int!
    var description: String {
        return "corners: \(corners)"
    }
}


class SubClass: SuperClass {
    override init() {  //重载构造器
        super.init()
        corners = 5
    }
}

class MainClass {
    var no1 : Int
    init(no1 : Int) {
        self.no1 = no1
    }
}

class BabyClass : MainClass {
    var no2 : Int
    init(no1 : Int, no2 : Int) {
        self.no2 = no2
        super.init(no1:no1)
    }
    // 便利方法, 可以减少参数， 然后在该方法里设置默认参数
    override convenience init(no1: Int)  {
        self.init(no1:no1, no2:0)
    }
}
