//
//  main.swift
//  Operation
//
//  Created by CavanSu on 2018/11/2.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// MARK: 算术运算符


// MARK: 比较运算符


// MARK: 逻辑运算符, &&、||、!


// MARK: 位运算符
var A = 60 // 二进制为 0011 1100
var B = 13 // 二进制为 0000 1101

print("A&B 结果为：\(A&B)")
print("A|B 结果为：\(A|B)")
print("A^B 结果为：\(A^B)")
print("~A 结果为：\(~A)")

var C = 4 // 二进制为 0000 0100
var n = 2
print("C << 2:\(C << n)") // 向左移动指定的位数, C = C * 2 * n
print("C >> 2:\(C >> n)") // 向左移动指定的位数, C = C / (2 * n)


// MARK: 区间运算符
print("\n闭区间运算符:")
for index in 1...5 {
    print("\(index) * 5 = \(index * 5)")
}

print("\n半开区间运算符:")
for index in 1..<5 {
    print("\(index) * 5 = \(index * 5)")
}


// MARK: 一元，二元，三元
func yuan() {
    let A = 1
    let B = 2
    let C = true
    let D = false
    print("-A 的值为：\(-A)")
    print("A + B 的值为：\(A + B)")
    print("三元运算：\(C ? A : B )")
    print("三元运算：\(D ? A : B )")
}

