//
//  ViewController.swift
//  ErrorCatch
//
//  Created by CavanSu on 2018/11/21.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import UIKit

enum MetLogError: Error {
    case fail(String)
    case invalidParameter(String)
}

extension MetLogError {
    var localizedDescription: String {
        switch self {
        case .fail(let reason):             return "\(reason)"
        case .invalidParameter(let para):   return "\(para)"
        }
    }
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try test()
        } catch let error as MetLogError {
            print("error: \(error.localizedDescription)")
        } catch {
            print("error: \(error.localizedDescription)")
        }
        
        do {
            try test1()
        } catch let error as MetLogError {
            print("error: \(error.localizedDescription)")
        } catch {
            print("error: \(error.localizedDescription)")
        }
    }
    
    func test() throws {
        try block { (result) in
            if result {
                throw MetLogError.fail("error in block of test")
            }
        }
    }

    func test1() throws {
        try block(didFinish: nil)
    }
    
    func block(didFinish: ((_ success: Bool) throws -> Swift.Void)? = nil) throws {
        if let finish = didFinish {
           try finish(true)
        } else {
            throw MetLogError.fail("error in block")
        }
    }
}

