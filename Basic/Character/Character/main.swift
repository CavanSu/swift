//
//  main.swift
//  Character
//
//  Created by CavanSu on 2018/11/22.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// MARK:
let char1: Character = "A"
let char2: Character = "B"

print("char1: \(char1)")
print("char2: \(char2)")


// Swift 中以下赋值会报错, 超过长度
//let char: Character = "AB"


// MARK: Swift 中不能创建空的 Character（字符） 类型变量或常量：
//let char1: Character = ""
//var char2: Character = ""


// MARK: 遍历字符串中的字符
for ch in "Runoob".characters {
    print(ch)
}


// MARK: 字符串连接字符
var varA:String = "Hello "
let varB:Character = "G"

varA.append( varB )

print("varC  =  \(varA)")
