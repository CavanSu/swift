//
//  main.swift
//  19- Protocol
//
//  Created by CavanSu on 22/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

/*
 struct SomeStructure: FirstProtocol, AnotherProtocol {
 // 结构体内容
 }
 
 class SomeClass: SomeSuperClass, FirstProtocol, AnotherProtocol {
 // 类的内容
 }
 */


class Student: PrintProtocol {
    var name: String?
    
    func printSelf() {
        if let name = name {
            print("name: \(name)")
        }
    }
}


let s1 = Student()
s1.name = "Cavan"
s1.printSelf()


var day = Days.mon
day.showNextDay()
day.showNextDay()
day.showNextDay()
