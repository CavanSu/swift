//
//  Protocol.swift
//  19- Protocol
//
//  Created by CavanSu on 22/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// 1.
protocol PrintProtocol {
    func printSelf()
}


// 2.
protocol DayOfWeek {
   mutating func showNextDay()
}

// mutating -> 值类型(结构体，枚举)的实例方法中，将mutating关键字作为函数的前缀，写在func之前，表示可以在该方法中修改它所属的实例及其实例属性的值。
enum Days: DayOfWeek {
    case sun, mon, tue, wed, thurs, fri, sat
    mutating func showNextDay() {
        switch self {
        case .sun:
            self = .mon
            print("Monday")
        case .mon:
            self = .tue
            print("Tuesday")
        case .tue:
            self = .wed
            print("Wednesday")
        case .wed:
            self = .thurs
            print("Thursday")
        case .thurs:
            self = .fri
            print("Friday")
        case .fri:
            self = .sat
            print("Saturday")
        case .sat:
            self = .sun
            print("Sunday")
        }
    }
}


// 3.
protocol InitProtocol {
    init(description: String)
    
    // 协议中声明的属性, 必须要给出, get, get set
    var age: Int! {get}
    var name: String! {get set}
}

class BaseClass: InitProtocol {
    // 因为 InitProtocol 中有 构造方法，且 BaseClass 遵守这个协议， 那么 init(description: String) 前要添加 required 关键字
    // 而且其 SubClass 也会遵守 SuperClass 准守的协议， 所以下面的 SubClass 也实现 init(description: String)， 并添加 required
    required init(description: String) {
        
    }
    
    // 实现协议的属性的get
    var age: Int! {
        return 10
    }
     // 实现协议的属性的get set
    var name: String! = "ASD"
}

class SubClass: BaseClass {
    required init(description: String) {
        super.init(description: description)
    }
}



// 4.
// 使用required修饰符可以保证：所有的遵循该协议的子类，同样能为构造器规定提供一个显式的实现或继承实现。
// 如果一个子类重写了父类的指定构造器，并且该构造器遵循了某个协议的规定，那么该构造器的实现需要被同时标示required和override修饰符：

protocol tcpprotocol {
    init(no1: Int)
}

class mainClass {
    var no1: Int
    init(no1: Int) {
        self.no1 = no1 // 初始化
    }
}

class sub1Class: mainClass {
    override init(no1: Int)  {
        super.init(no1: no1)
    }
}

class sub2Class: mainClass {
    var no2: Int
    override init(no1: Int)  {
        no2 = 0
        super.init(no1: no1)
    }
}

class sub3Class: mainClass, tcpprotocol {
    var no2: Int
    init(no1: Int, no2 : Int) {
        self.no2 = no2
        super.init(no1:no1)
    }
    // 因为遵循协议，需要加上"required"; 因为继承自父类，需要加上"override"， 因为是简易构造方法，添加 convenience
    required override convenience init(no1: Int)  {
        self.init(no1:no1, no2:0)
    }
}



// 5. 类专属协议
/*
 
 可以在协议的继承列表中,通过添加class关键字,限制协议只能适配到类（class）类型。
 该class关键字必须是第一个出现在协议的继承列表中，其后，才是其他继承协议。格式如下：
 
 protocol SomeClassOnlyProtocol: class, SomeInheritedProtocol {
 // 协议定义
 }
 
 */



