//
//  main.swift
//  OptionalChaining
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

class Person {
    var residence: Residence?
    init() {
        residence = Residence()
    }
}

class Residence {
    var numberOfRooms = 1
    var room : Room?
    init() {
        room = Room()
    }
}

class Room {
    var hasLight: Bool?
    init() {
        hasLight = true
    }
}

let john = Person()

// Error
//let roomCount = john.residence!.numberOfRooms


if let _ = john.residence?.numberOfRooms {
    print("roomCount")
}
else {
    print("no exit")
}


if let _ = john.residence?.room {
    print("room")
}
else {
    print("no exit")
}


if let _ = john.residence?.room?.hasLight {
    print("hasLight")
}
else {
    print("no exit")
}

