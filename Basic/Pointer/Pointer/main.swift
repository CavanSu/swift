//
//  main.swift
//  Pointer
//
//  Created by CavanSu on 2019/1/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import Foundation
import Accelerate

// Array of Swift is a struct
class CavModel: NSObject {
    var count : Int
    
    override init() {
        count = 1
    }
}

// MARK: 打印地址
func printAddress() {
    let model = CavModel()
    print("address: \(Unmanaged<AnyObject>.passUnretained(model as AnyObject).toOpaque())")
}

func pointerUsedInArray() {
    let array : [CavModel] = [CavModel(), CavModel(), CavModel(), CavModel(), CavModel()]
    
    for (index, item) in array.enumerated() {
        print("在 index = \(index) 位置上的count值为 \(item.count)")
    }
    print("\n")
    
    let size = MemoryLayout<CavModel>.size
    // 定义一个指针变量 pointer, UnsafeMutablePointer<指针要指向的类型> allocate(capacity: 指针要指向的类型的大小)
    let pointer = UnsafeMutablePointer<CavModel>.allocate(capacity: size)
    
    // 指向数组的第4个对象
    pointer.pointee = array[3]
    print("array[3] : \(array[3].count)")
    
    // 修改指针指向的对象的成员属性
    pointer.pointee.count = 12
    
    for (index, item) in array.enumerated() {
        print("在 index = \(index) 位置上的count值为 \(item.count)")
    }
}

func test1() {
    // 对于一个 UnsafePointer<T> 类型，我们可以通过 pointee 属性对其进行取值，
    func incrementor(pointer: UnsafePointer<Int>) {
        print("value: \(pointer.pointee)")
    }
    
    var x = 10
    incrementor(pointer: &x)
    
    // 如果这个指针是可变的 UnsafeMutablePointer<T> 类型，我们还可以通过 pointee 对它进行赋值
    func incrementor1(pointer: UnsafeMutablePointer<Int>) {
        pointer.pointee += 1
    }

    var a = 10
    incrementor1(pointer: &a)
    
    print("a: \(a)")
    
    // 与这种做法类似的是使用 Swift 的 inout 关键字。我们在将变量传入 inout 参数的函数时，
    // 同样也使用 & 符号表示地址。不过区别是在函数体内部我们不需要处理指针类型，而是可以对参数直接进行操作
    func incrementor2(num: inout Int) {
        num += 1
    }

    var b = 10
    incrementor2(num: &b)
    print("b: \(b)")
    
    // &a 仅在传参时有效，赋值会编译不通过
    // let b = &a , error
}

func test2() {
    // 指针初始化和内存管理
    // 在 Swift 中不能直接取到现有对象的地址，我们还是可以创建新的 UnsafeMutablePointer 对象。
    // 与 Swift 中其他对象的自动内存管理不同，对于指针的管理，是需要我们手动进行内存的申请和释放的。一个 UnsafeMutablePointer 的内存有三种可能状态：
    // 1.内存没有被分配，这意味着这是一个 null 指针，或者是之前已经释放过
    // 2.内存进行了分配，但是值还没有被初始化
    // 3.内存进行了分配，并且值已经被初始化
    
    // 其中只有第三种状态下的指针是可以保证正常使用的。
    // UnsafeMutablePointer 的初始化方法 (init) 完成的都是从其他类型转换到 UnsafeMutablePointer 的工作。
    // 我们如果想要新建一个指针，需要做的是使用 allocate(capacity:) 这个类方法。
    // 该方法根据参数 capacity: Int 向系统申请 capacity 个数的对应泛型类型的内存。下面的代码申请了一个 Int 大小的内存，并返回指向这块内存的指针
    
    let a = 10
    let size = MemoryLayout<Int>.size
    let pointer = UnsafeMutablePointer<Int>.allocate(capacity: size)
    pointer.initialize(to: a)
    
    print("pointer.pointee: \(pointer.pointee)")
    
    pointer.pointee = 13
 
    print("pointer.pointee: \(pointer.pointee)")
    
    pointer.deinitialize(count: size)
    pointer.deallocate()
}

func test3() {
    // let 修饰的数组，对应的指针类型为 UnsafePointer
    let a: [Float] = [1, 2, 3, 4]
    let b: [Float] = [0.5, 0.25, 0.125, 0.0625]
    let size = MemoryLayout<Float>.size
    
    // var 修饰的数组，对应的指针类型为 UnsafeMutablePointer
    var result: [Float] = [0, 0, 0, 0]
    
    vDSP_vadd(a, 1,
              b, 1,
              &result, 1, 4)
              
    print("result: \(result)")
}

func test4() {
    var array = [1, 2, 3, 4, 5]
    let arrayPointer = UnsafeMutableBufferPointer<Int>(start: &array,
                                                       count: array.count)
    
    // baseAddress 是第一个元素的指针，类型为 UnsafeMutablePointer<Int>
    if let basePointer = arrayPointer.baseAddress {
        print("first: \(basePointer.pointee)")  // 1
        basePointer.pointee = 10
        print("first: \(basePointer.pointee)") // 10
        
        //下一个元素
        let nextPointer = basePointer.successor()
        print("second: \(nextPointer.pointee)") // 2
        
        //下一个元素
        print("second: \((basePointer + 1).pointee)") // 2
    }
}

func pointer2() {
    // 初始化一个数组
    var num: [UInt] = [1,2,3,4,5]
    
    // 定义一个不变指针类型为UInt
    // 当然还有可变的指针UnsafeMutablePointer 可变和不变顾名思义 指的是指向的内存能不能改变
    var p: UnsafeMutablePointer<UInt>
    
    // 取数组的指针
    // arrayPtr就是指向num的一段连续的内存的指针
    let arrayPtr = UnsafeMutableBufferPointer<UInt>(start: &num, count: num.count)
    // 取数组首地址
    p = arrayPtr.baseAddress as! UnsafeMutablePointer<UInt>
    
    (p+1).pointee = 20
    print("p[1] === \((p+1).pointee)")  //打印内存的值
    
    for (index, item) in num.enumerated() {
        print("在 index = \(index) 位置上的count值为 \(item)")
    }
}

func pointer3() {
    // 初始化一个数组
    var array : [CavModel] = [CavModel(), CavModel(), CavModel(), CavModel(), CavModel()]
    
    var p : UnsafeMutablePointer<CavModel>
    
    let arrayPointer = UnsafeMutableBufferPointer<CavModel>(start: &array, count: array.count)
    p = arrayPointer.baseAddress as! UnsafeMutablePointer<CavModel>
    (p + 1).pointee.count = 20
    
    for (index, item) in array.enumerated() {
        print("在 index = \(index) 位置上的count值为 \(item.count)")
    }
}

//test1()
//test2()
//test3()
test4()
