//
//  main.swift
//  18- Extension
//
//  Created by CavanSu on 22/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation
/*
extension SomeType {
    // 加到SomeType的新功能写到这里
}
 
// 让某个对象遵守某个协议，协议实现的方法写在下面
extension SomeType: SomeProtocol, AnotherProctocol {
// 协议实现写到这里
}
 
*/


extension Int {
    var add: Int {return self + 100 }
    var sub: Int { return self - 10 }
    var mul: Int { return self * 10 }
    var div: Int { return self / 5 }
}

let addition = 3.add
print("加法运算后的值：\(addition)")

let subtraction = 120.sub
print("减法运算后的值：\(subtraction)")

let multiplication = 39.mul
print("乘法运算后的值：\(multiplication)")

let division = 55.div
print("除法运算后的值: \(division)")

let mix = 30.add + 34.sub
print("混合运算结果：\(mix)")

