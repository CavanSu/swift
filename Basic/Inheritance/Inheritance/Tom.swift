//
//  Tom.swift
//  12- Inheritance
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Cocoa

class Tom: StudDetails {
    
    init() {
        super.init(stname: "Tom", mark1: 99, mark2: 80, mark3: 70)
    }
    
    // 重写 superClass 的init方法
//    override init(stname: String, mark1: Int, mark2: Int, mark3: Int) {
//    }
    
    func show() {
        print("Tom\n")
        print(stname)
        print(mark1)
        print(mark2)
        print(mark3)
    }
}
