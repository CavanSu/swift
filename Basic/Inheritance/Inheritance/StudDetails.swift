//
//  StudDetails.swift
//  12- Inheritance
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Cocoa

class StudDetails: NSObject {
    var stname: String
    var mark1: Int
    var mark2: Int
    var mark3: Int
    
    init(stname: String, mark1: Int, mark2: Int, mark3: Int) {
        self.stname = stname
        self.mark1 = mark1
        self.mark2 = mark2
        self.mark3 = mark3
    }
}
