//
//  Circle.swift
//  12- Inheritance
//
//  Created by CavanSu on 21/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

class Circle {
    // 添加final 将防止被重写
//    final var radius = 12.5
    var radius = 12.5

    var bounrdWidth: Int!
    
    var area: String {
        return "矩形半径 \(radius) "
    }
}

// 继承超类 Circle
class Rectangle: Circle {
    var print = 7
    
    // SupClass 的属性为 只读 时， 该属性才重写
    override var area: String {
        return super.area + " ，但现在被重写为 \(print)"
    }
}

class Square: Rectangle {
    // 重写属性观察器
    override var radius: Double {
        didSet {
            print = Int(radius / 5.0)+1
        }
    }
}
