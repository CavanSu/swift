//
//  main.swift
//  Condition
//
//  Created by CavanSu on 2018/11/2.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// MARK: if else


// MARK: 循环

// for
for i in 1...10 {
    print("i: \(i)")
}


// while
var a = 0
while a < 5 {
    a += a
    print("a: \(a)")
}


// MARK: switch
var www = 3
switch www {
case 1:
    print("switch 1")
case 2:
    print("switch 2")
case 3:
    print("switch 3")
     fallthrough // 如果在一个case执行完后，继续执行下面的case，需要使用fallthrough(贯穿)关键字。
default:
    print("switch default")
}
