//
//  main.swift
//  02-DataType
//
//  Created by CavanSu on 2018/11/2.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// 类型别名 typealias
typealias CAInt = Int

let a: Int = 2
let b: CAInt = 3
let c: Int = a + b

print("c: \(c)")


// 类型推断
// varA 会被推测为 Int 类型
var varA = 42
print(varA)

// varB 会被推测为 Double 类型
var varB = 3.14159
print(varB)

// varC 也会被推测为 Double 类型
var varC = 3 + 0.14159
print(varC)
