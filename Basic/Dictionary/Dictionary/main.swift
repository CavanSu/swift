//
//  main.swift
//  Dictionary
//
//  Created by CavanSu on 2019/1/2.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import Foundation

// 1. Init
func initDic() {
    // Key: Int, Value: String.
    var _ = [Int: String]()
    let _: [Int: String] = [1: "one", 2: "two", 3: "three"]
}

// 2. 可以根据字典的索引来访问数组的元素
func readDic() {
    let someDict2: [Int: String] = [1:"one", 2:"two", 3:"three"]
    print("someDict2, index 2 = \(someDict2[2]!)")
}

func modifiedDic() {
    // 3. 添加KV对
    var someDict = [Int: String]()
    
    someDict[0] = "zero"
    someDict[1] = "one"
    someDict[2] = "two"
    print("\n")
    // 遍历字典
    for (key, value) in someDict {
        print("Key: \(key), Value: \(value)")
    }
    
    // 4. 修改字典
    someDict.updateValue("two,tow", forKey: 2)
    someDict[1] = "one, one"
    
    print("\n")
    for (key, value) in someDict {
        print("Key: \(key), Value: \(value)")
    }
    
    // 5. 移除KV对
    someDict.removeValue(forKey: 2)
    someDict[1] = nil // 通过nil 来移除 KV
    print("\n")
    
    for (key, value) in someDict {
        print("Key: \(key), Value: \(value)")
    }
}

// 6. 字典转换为数组
func dicConvertedToArray() {
    let someDict2: [Int: String] = [1: "one", 2: "two", 3: "three"]
    let dictKeys = [Int](someDict2.keys)
    let dictValues = [String](someDict2.values)
    
    for key in dictKeys {
        print("Key Array: \(key)")
    }
    
    for value in dictValues {
        print("Value Array: \(value)")
    }
    
    // count
    print("count: \(someDict2.count)")
    
    // isEmpty
    print("isEmpty: \(someDict2.isEmpty)")
}

func traversal() {
    let someDict3: [Int: String] = [1: "one", 2: "two", 3: "three"]
    for (key, value) in someDict3 {
        print("key: \(key)")
        print("value: \(value)")
    }
}




//        initDic()
//        readDic()
modifiedDic()
