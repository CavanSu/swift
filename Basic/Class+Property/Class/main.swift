//
//  main.swift
//  10- Class
//
//  Created by CavanSu on 20/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

let student = Student(age: "13")
student.score = 123
print("score: \(student.score!)")

let student2 = student // 是指向，而不是拷贝
student2.score = 321
print("score: \(student.score!)")
print("score: \(student2.score!)")

print("student: \(Unmanaged<AnyObject>.passUnretained(student as AnyObject).toOpaque())")
print("student2: \(Unmanaged<AnyObject>.passUnretained(student2 as AnyObject).toOpaque())")

print("student.name: \(student.name)")

student.age = "12"
print("student.age: \(student.age)")











// MARK: - 恒等运算符, ===
let spClass1 = SampleClass(s: "Hello")
let spClass2 = SampleClass(s: "Hello")
let spClass3 = spClass1


if spClass1 === spClass2 { // false
    print("引用相同的对象 \(spClass1)")
}

if spClass1 !== spClass2 { // true
    print("引用不相同的对象 \(spClass2)")
}

if spClass1 === spClass3 { // true
    print("恒等运算符 ===， 判断是否是同一个对象")
}

// Equatable
if spClass1 == spClass2 {
    print("spClass1 与 spClass2 myProperty 相同")
}
