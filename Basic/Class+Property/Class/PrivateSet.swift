//
//  PrivateSet.swift
//  Class
//
//  Created by CavanSu on 2019/5/15.
//  Copyright © 2019 CavanSu. All rights reserved.
//

import Cocoa

class PrivateSet: NSObject {
    // 外界为可读不可写
    private(set) var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func update(_ name: String) {
        self.name = name
    }
}
