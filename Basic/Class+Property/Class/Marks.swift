//
//  Student.swift
//  10- Class
//
//  Created by CavanSu on 20/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// MARK: - 成员属性

class Marks: NSObject {

    // MARK: - 存储属性:
    // 存储属性可以是变量存储属性（用关键字var定义），也可以是常量存储属性（用关键字let定义）。
    let name = "Marks"
    
    // 懒加载 / 延迟存储属性
    // 第一次需要用到时才创建
    lazy var tag = NSObject()
    
    
    // MARK: - 计算属性
    
    // 假设， 成绩都是0
    var math: Int = 0 
    var english: Int = 0
    var sum: Int {
        get {
            // return sum  切记会 不停来到 get 方法，造成死循环
            return math + english
        }
    }
    
    // 假设， 排最后一名
    var rank = 0
    
    // 计算属性不直接存储值，而是提供一个 getter 来获取值，一个可选的 setter 来间接设置其他属性或变量的值。
    var sumAndRank: (Int, Int) {
        get {
            return (sum, rank)
        }
        
        set(sr) {
            // sum = sr.0  , 只有get 方法， 无法赋值
            rank = sr.1
        }
    }
    
    // 只读计算属性
    // 只有 getter 没有 setter 的计算属性, 如上面的 sum 与x 下面的 sumStringType
    var sumStringType: String {
        return "\(sum)"
    }
    
    
    // MARK: - 属性观察器
    
    // willSet在设置新的值之前调用
    // didSet在新的值被设置之后立即调用
    // willSet和didSet观察器在属性初始化过程中不会被调用
    
    var extra: String? {
        willSet {
            print("newValue: \(String(describing: newValue))")
        }
        didSet {
            print("oldValue: \(String(describing: oldValue))")
            print("currentValue: \(String(describing: extra))")
        }
    }
    
    
    // MARK: - 类型属性 / 静态属性
    static var hello = "hello"
    static let world = "world"
}
