//
//  Teacher.swift
//  10- Class
//
//  Created by CavanSu on 20/03/2018.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Foundation

// MARK: - 成员属性 后加 ?,！ 的问题

class Teacher: NSObject {
    // 当属性在初始化时无值，需加 ?, !
    var tId: Int!
    var name: Int!
}

class Student: NSObject {
    // 当属性在初始化时无值，需加 ?, !
    var score: Int!
    
    // 当属性在初始化时有值，无需加 ?, !
    var subj1: Double = 0
    var subj2: Double = 0
    
    var age: String
    
    var name: String {
        get {
            print("overwrite get name")
            return "Studen.name" // 只能返回其他指回去
        }
    }
    
    init(age: String) {
        self.age = age
    }
}

struct TeacherStruct {
    // struct 属性虽然可以不加 ?,!
    // 但是 需要采用 带参数的初始化方法 let teacher = TeacherStruct(tId: 1, name: 2)
    
    // 总之: 当属性在初始化时无值，需加 ?, !
    // 当属性在初始化时有值，无需加 ?, !
    var tId: Int
    var name: Int
}
