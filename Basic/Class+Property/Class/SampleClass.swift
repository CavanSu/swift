//
//  SampleClass.swift
//  10- Class
//
//  Created by CavanSu on 2018/11/24.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Cocoa

class SampleClass: Equatable {
    let myProperty: String
    init(s: String) {
        myProperty = s
    }
    
    // 实现 Equatable 协议中的方法
    // 使两个对象的 myProperty 相同时， 被判定为相同值
    static func ==(lhs: SampleClass, rhs: SampleClass) -> Bool {
        return lhs.myProperty == rhs.myProperty
    }
}
