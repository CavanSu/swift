//
//  LazyLoad.swift
//  Class
//
//  Created by CavanSu on 2018/11/24.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import Cocoa

// MARK: - 复杂的懒加载方法
class Example : NSObject {
    
    // 当懒加载只需一个初始化方法就可以时，可以使用下面的方法
    lazy var balala = Person(name: "balala", age: 25)
    
    // 也可以通过一个无参数的闭包在需要时，进行初始化
    lazy var jolin: Person = {
        return Person(name: "jolin", age: 23)
    }()
    
    // 当初始化需要多行，或者需要代入参数时可以， 通过一个带参数的闭包在需要时，进行初始化
    lazy var kangkang: Person = { (name: String, age: Int) -> Person in
        let kk = Person(name: name, age: age)
        kk.extra = "nothing"
        return kk
    }("kangkang", 12)
}

class Person: NSObject {
    var name: String
    var age: Int
    var extra: String?
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}
