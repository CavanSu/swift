//
//  CSNavigationController.swift
//
//
//  Created by CavanSu on 2018/6/29.
//  Copyright © 2018 CavanSu. All rights reserved.
//

import UIKit

protocol CSNavigationControllerDelegate: NSObjectProtocol {
    func navigationDidBackButtonClicked(navigation: CSNavigationController, from: UIViewController, to: UIViewController?)
}

extension CSNavigationControllerDelegate {
    func navigationDidBackButtonClicked(navigation: CSNavigationController, from: UIViewController, to: UIViewController?) { }
}

class CSNavigationController: UINavigationController {
    var backButton: UIButton?
    weak var csDelegate: CSNavigationControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if let backButton = backButton {
            viewController.navigationItem.setHidesBackButton(true, animated: false)
            backButton.isHidden = false
        }
        
        if let titleCenter = self.navigationItem.titleView?.center {
            updateBackButtonCenterY(y: titleCenter.y)
        }
        
        super.pushViewController(viewController, animated: animated)
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        let from = self.children.last
        let toIndex = self.children.count - 2
        var to: UIViewController?
        if toIndex >= 0 {
            to = self.children[toIndex]
        } else {
            to = nil
        }
    
        csDelegate?.navigationDidBackButtonClicked(navigation: self, from: from!, to: to)
        
        super.popViewController(animated: animated)
        return to
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

private extension CSNavigationController {
    func setupBarClearColor() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
    }
    
    func setupBarOthersColor(color: UIColor) {
        self.navigationBar.barTintColor = color
    }
    
    func setupTitleFontSize(fontSize: UIFont) {
        if var attributes = self.navigationBar.titleTextAttributes {
            attributes[NSAttributedString.Key.font] = fontSize
            self.navigationBar.titleTextAttributes = attributes
        } else {
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: fontSize]
        }
    }
    
    func setupTitleFontColor(color: UIColor) {
        if var attributes = self.navigationBar.titleTextAttributes {
            attributes[NSAttributedString.Key.foregroundColor] = color
            self.navigationBar.titleTextAttributes = attributes
        } else {
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
        }
    }
    
    func setupTintColor(color: UIColor) {
        self.navigationBar.tintColor = color
    }
    
    func setupBackButton(image: UIImage) {
        self.navigationItem.hidesBackButton = true
        
        let x = CGFloat(15)
        let w = CGFloat(12 * 2)
        let h = CGFloat(21 * 1.5)
        let y = CGFloat(13 + 19.5 * 0.5 - h * 0.5)
        let backButton = UIButton(frame: CGRect(x: x, y: y, width: w, height: h))
        backButton.setImage(image, for: .normal)
        
        backButton.imageView?.contentMode = .scaleAspectFill
        backButton.isHidden = true
        backButton.addTarget(self, action: #selector(popBack), for: .touchUpInside)
        self.navigationBar.addSubview(backButton)
        self.backButton = backButton
    }
    
    func updateBackButtonCenterY(y: CGFloat) {
        guard let backButton = backButton else {
            return
        }
        var backButtonCenter = backButton.center
        backButtonCenter.y = y
        backButton.center = backButtonCenter
    }
    
    @objc func popBack() {
        if let vc = self.viewControllers.last {
            vc.navigationController?.popViewController(animated: true)
        }
    }
}
